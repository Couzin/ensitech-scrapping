from clean.clean_markups import clean_markup as clean

medals_correspondance = {
    "1": "gold",
    "2": "silver",
    "3": "bronze"
}


def format_athlet_palmares(medals: list, amount: list, athlet, summary):
    """
    Format the palmares of an athlet.
    :param medals: list of medals
    :param amount: list of amount of medals
    :param athlet: name of the athlet
    :param summary: summary of the athlet
    :return: an array of objects containing the athlet and his palmares
    """
    amount.pop(0)  # removing the first element which is the total amount of medals
    for i in range(len(medals)):
        medal_type = str(medals[i]['data-medal'])
        print(medal_type, medals[i])
        medals[i] = medals_correspondance[medal_type]
    palmares = {medals[i]: clean(str(amount[i])) for i in range(len(medals))}
    print(palmares)
    return {'name': athlet, 'summary': summary, "palmares": palmares}
