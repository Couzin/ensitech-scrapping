import re

no_markup = re.compile(r'<.*?>')


def clean_markup(text):
    return re.sub(no_markup, '', text)
