# Olympic Scrap

## Description

This project is a web scraping project that scrapes [this](https://olympics-statistics.com/olympic-athletes/) website, in purpose to filter and model datas.

## Installation

This project is using Python with the following libraries:
- requests
- bs4

To install packages you can either run the project in a Pipen (pip virtual environment) or install packages globally.

Then you can run the project by running the following command:
```bash
python main.py
```

Depending on your Python installation, you might need to use `python3` instead of `python`.

## Infos

In order to prevent the website from being overloaded, the script is waiting 1 second between each request. This result as a very long execution time.
