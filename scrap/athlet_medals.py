import json
import string
import time

import requests
from bs4 import BeautifulSoup
from format.format_athlet_palmares import format_athlet_palmares

"""
example of athlete card
<a class="card athlet visible" href="/olympic-athlete/Peter-Abay/24156">
<div class="bez">
<div class="vn">Péter<svg class="male" style="enable-background:new 0 0 32 32;" version="1.1" viewbox="0 0 32 32" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:
xlink="http://www.w3.org/1999/xlink" y="0px">
<g>
<path d="M25.3,19.8c0,1-0.8,1.7-1.7,1.7s-1.7-0.8-1.7-1.7v-6.4h-1.2V30c0,1.1-0.9,2-2,2c-1.1,0-2-0.9-2-2v-8.4h-1.2V30
                c0,1.1-0.9,2-2,2s-2-0.9-2-2V13.4h-1.2v6.4c0,1-0.8,1.7-1.7,1.7s-1.7-0.8-1.7-1.7v-7.6c0-1.9,1.6-3.5,3.5-3.5h11.6
                c1.9,0,3.5,1.6,3.5,3.5V19.8z M16,8.1c-2.3,0-4.1-1.8-4.1-4.1S13.7,0,16,0s4.1,1.8,4.1,4.1S18.3,8.1,16,8.1z"></path>
</g>
</svg></div>
<div class="nn">
<div class="n">Abay</div>
</div>
</div>
<img alt="" src="/media/flagge/hu.png"/>
</a>
"""


def get_soup(url):
    response = requests.get(url)
    response.raise_for_status()
    return BeautifulSoup(response.content, 'html.parser')


def get_urls(letter):
    """
    This function returns the urls of the athletes whose name starts with the given letter
    :param letter: letter of the alphabet for the name of the athletes
    :return: array of urls
    """
    print("saving page for athletes starting with letter %s", letter)
    soup = get_soup(f'https://olympics-statistics.com/olympic-athletes/{letter}')
    urls = []
    athletes_section = soup.find('div', {'class': 'deck max all'})
    # print(athletes_section)
    for athlete in athletes_section.find_all('a', {'class': 'card athlet visible'}):
        url = athlete['href']
        urls.append(url)
    return urls


# Function to extract athletes and their palmares
def get_athletes_and_palmares():
    """
    :return: an array of objects containing athletes and their palmares
    """
    for letter in string.ascii_lowercase:
        urls = ''
        urls = get_urls(letter)

        athlets = []
        for url in urls:
            try:
                time.sleep(0.5)  # To avoid being blocked
                print(f'https://olympics-statistics.com{url}')
                soup = get_soup(f'https://olympics-statistics.com{url}')
                athlet = soup.find('h1').text
                medal = soup.find('h2').text
                medal_markup = soup.find('div', {'class': 'teaser'})
                medal_types = medal_markup.find_all('div', {'class': 'the-medal'})
                medal_amounts = medal_markup.find_all('span', {'class': 'mal'})
                athlets.append(format_athlet_palmares(medal_types, medal_amounts, athlet, medal))
            except Exception as E:
                # if any error occurs, we print it and just continue
                print(E)
                continue
        if letter == 'a':
            # for the first letter, we create the file
            with open(f'./results/athlets/athlets.json', 'x', encoding='utf-8') as file:
                json.dump(athlets, file, ensure_ascii=False, indent=4)
        # else we append to the file
        with open(f'./results/athlets/athlets.json', 'a', encoding='utf-8') as file:
            json.dump(athlets, file, ensure_ascii=False, indent=4)
